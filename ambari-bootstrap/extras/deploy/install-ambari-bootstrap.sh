#!/usr/bin/env bash

sudo yum -y -q install git
sudo git clone git@gitlab.com:keitaf/keiba.git /opt/keiba
sudo chmod -R g+rw /opt/keiba/ambari-bootstrap
sudo chown -R ${USER}:users /opt/keiba/ambari-bootstrap
ln -s /opt/keiba/ambari-bootstrap ~/
